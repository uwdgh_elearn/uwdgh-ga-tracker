# README #

This README document contains steps necessary to use the _UW DGH | Google Analytics Tracker_ WordPress plugin

## What is this repository for? ##

* Adds your Google Analytics Tracking Code to your WordPress site using Google's Global Site Tag methodology.

## How do I get set up? ##

* Navigate to your wp-content/plugins/ folder
* `git clone https://bitbucket.org/uwdgh_elearn/uwdgh-ga-tracker`
Alternatively you can use the [Github Updater](https://github.com/afragen/github-updater) plugin to manage themes and plugins not hosted on WordPress.org.

## Who do I talk to? ##

* dghweb@uw.edu
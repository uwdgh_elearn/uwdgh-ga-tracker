=== UW DGH | Google Analytics Tracker ===
Contributors: dghweb, jbleys
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Tested up to: 6.7
Requires at least: 3.2

Adds your Google Analytics Tracking Code to your WordPress site using Google's Global Site Tag methodology.


== Description ==
Adds your Google Analytics Tracking Code to your WordPress site using Google's Global Site Tag methodology.

Supports Google Universal Analytics Tracking Id (GA_TRACKING_ID) and Google Analytics 4 Measurement ID (GA_MEASUREMENT_ID),
and an additional Google Conversion Tracking Id (AW-CONVERSION_ID).


== Changelog ==
**[unreleased]**

#### 0.2.2 / 2025-02-13
* Added data-cookie-consent="tracking" for integration with FreePrivacyPolicy.com Cookie Consent
* Tested with WordPress 6.7

#### 0.2.1 / 2024-09-21
* Tested with WordPress 6.6

#### 0.2 / 2023-12-21
* Refactored the disable tracking options to configure the gtag send_page_view config option.

#### 0.1.12 / 2023-12-21
* Tested with WordPress 6.4
* Add options to disable tracking for logged-in users

#### 0.1.11 / 2023-06-26
* PHP 8.x ready

#### 0.1.10 / 2023-06-07
* Bug fixes
* WP 6.2 ready

#### 0.1.9 / 2022‑03‑04
* Bug fixes

#### 0.1.8 / 2021‑06‑22
* Updated labels for Google Analytics 4 Measurement ID

#### 0.1.7 / 2020‑11‑20
* Added field for Conversion Label

#### 0.1.6 / 2020‑08‑13
* Added plugin icon

#### 0.1.5 / 2020‑08‑13
* Added changelog and plugin banner

#### 0.1.4 / 2020‑07‑22
* Added "Enable tracking" option

#### 0.1.3 / 2020‑06‑05
* Added field for conversion tracking id

#### 0.1 / 2018‑11‑07
* Initial commit

<?php
/*
Plugin Name:  UW DGH | Google Analytics Tracker
Plugin URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-ga-tracker
Update URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-ga-tracker
Description:  Adds your Google Analytics Tracking Code to your WordPress site using Google's Global Site Tag methodology.
Author:       Department of Global Health - University of Washington
Author URI:   https://depts.washington.edu/dghweb/
Version:      0.2.2
License:      GPLv2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  uwdgh-ga-tracker
Domain Path:
Bitbucket Plugin URI:  https://bitbucket.org/uwdgh_elearn/uwdgh-ga-tracker

"UW DGH | Google Analytics Tracker" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

"UW DGH | Google Analytics Tracker" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "UW DGH | Google Analytics Tracker". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( ! defined( 'UWDGH_GATracker_PLUGIN' ) ) {
	define( 'UWDGH_GATracker_PLUGIN', plugin_basename( __FILE__ ) ); // Plugin
}

if ( !class_exists( 'UWDGH_GoogleAnalyticsTracker' ) ) {

  class UWDGH_GoogleAnalyticsTracker {

    /**
    * class initializaton
    */
    public static function init() {
      // register plugin settings
      register_setting('uwdgh_ga_tracker_options','uwdgh_ga_tracking_enabled', array('default' => 0,));
      register_setting('uwdgh_ga_tracker_options','uwdgh_ga_tracking_admin_disabled', array('default' => 1,));
      register_setting('uwdgh_ga_tracker_options','uwdgh_ga_tracking_user_disabled', array('default' => 0,));
      register_setting('uwdgh_ga_tracker_options','uwdgh_ga_tracking_id');
      register_setting('uwdgh_ga_tracker_options','uwdgh_ga_conversion_tracking_id');
      register_setting('uwdgh_ga_tracker_options','uwdgh_ga_conversion_label');
      // Add plugin settings link to Plugins page
      add_filter( 'plugin_action_links_'.UWDGH_GATracker_PLUGIN, array( __CLASS__, 'uwdgh_ga_tracker_add_settings_link' ) );
      // add menu item
      add_action( 'admin_menu' , array( __CLASS__, 'uwdgh_add_ga_tracker_setting' ));
      // add tracking code to head
      add_action( 'wp_head', array( __CLASS__, 'uwdgh_add_ga_tracker' ), 1 );
      // register uninstall hook
      if( function_exists('register_uninstall_hook') )
        register_uninstall_hook(__FILE__,array( __CLASS__, 'uwdgh_ga_tracker_uninstall' ));
    }

    /**
    * Add settings link
    */
    static function uwdgh_ga_tracker_add_settings_link( $links ) {
      $settings_link = '<a href="options-general.php?page=uwdgh-ga-tracker">' . __( 'Settings' ) . '</a>';
      array_push( $links, $settings_link );
      return $links;
    }

    /**
    * Admin menu item
    */
    static function uwdgh_add_ga_tracker_setting() {
      add_options_page('UW DGH | Google Analytics Tracker','UW DGH | Google Analytics Tracker','manage_options','uwdgh-ga-tracker',array( __CLASS__, 'uwdgh_ga_tracker_options_page' ));
    }

    /**
    * Options form
    */
    static function uwdgh_ga_tracker_options_page() { ?>
      <div class="wrap">
        <h2><?php _e('UW DGH | Google Analytics Tracker','uwdgh-ga-tracker');?></h2>
        <p><?php _e('Here you can set or edit the fields needed for the plugin.','uwdgh-ga-tracker');?></p>
        <form action="options.php" method="post" id="uwdgh-ga-tracker-options-form">
          <?php settings_fields('uwdgh_ga_tracker_options'); ?>
          <table class="form-table">
            <tr class="odd" valign="top">
              <th scope="row">
                <label for="uwdgh_ga_tracking_enabled">
                  <?php _e('Enable Google Analytics tracking','uwdgh-ga-tracker');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_ga_tracking_enabled" name="uwdgh_ga_tracking_enabled"  value="1" <?php checked(1, get_option('uwdgh_ga_tracking_enabled'), true); ?> />
                <span><em>(Default: unchecked)</em></span>
              </td>
            </tr>
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_ga_tracking_admin_disabled">
                  <?php _e('Disable pageview measurement for "administrators"','uwdgh-ga-tracker');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_ga_tracking_admin_disabled" name="uwdgh_ga_tracking_admin_disabled"  value="1" <?php checked(1, get_option('uwdgh_ga_tracking_admin_disabled'), true); ?> />
                <span><em>(Default: checked)</em></span>
								<p class="description">This option will set the <code>send_page_view</code> config setting to <code>false</code> for logged-in users whose role is 'administrator' or user has capability 'manage_options'.<br>
								See: <a href="https://developers.google.com/analytics/devguides/collection/ga4/views" target="_blank" rel="noopener noreferrer">Measure pageviews  |  Google Analytics 4 Properties</a>
								</p>
              </td>
            </tr>
            <tr class="odd" valign="top">
              <th scope="row">
                <label for="uwdgh_ga_tracking_user_disabled">
                  <?php _e('Disable pageview measurement for any logged-in user','uwdgh-ga-tracker');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_ga_tracking_user_disabled" name="uwdgh_ga_tracking_user_disabled"  value="1" <?php checked(1, get_option('uwdgh_ga_tracking_user_disabled'), true); ?> />
                <span><em>(Default: unchecked)</em></span>
								<p class="description">This option will set the <code>send_page_view</code> config setting to <code>false</code> for any user with any role who is logged in.<br>
								See: <a href="https://developers.google.com/analytics/devguides/collection/ga4/views" target="_blank" rel="noopener noreferrer">Measure pageviews  |  Google Analytics 4 Properties</a>
								</p>
              </td>
            </tr>
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_ga_tracking_id">
                  <?php _e('Google Analytics 4 Measurement ID (GA_MEASUREMENT_ID):','uwdgh-ga-tracker');?>
                </label>
              </th>
              <td>
                <input type="text" id="uwdgh_ga_tracking_id" name="uwdgh_ga_tracking_id" class="regular-text" value="<?php echo esc_attr(get_option('uwdgh_ga_tracking_id')); ?>" />
                <p class="description">e.g.: <s>"UA-XXXXXXX-X" or</s> "G-XXXXXXXX"</p>
								<p  class="description"><a href="https://support.google.com/analytics/answer/11583528?sjid=13211927553301664639-NC" target="_blank" rel="noopener noreferrer">Unviversal Analytics tracking Id's (GA_TRACKING_ID) are deprecated since July 1, 2023</a> and should not be used.</p>
              </td>
            </tr>
            <tr class="odd" valign="top">
              <th scope="row">
                <label for="uwdgh_ga_conversion_tracking_id">
                  <?php _e('Google Conversion Tracking Id (AW-CONVERSION_ID):','uwdgh-ga-tracker');?>
                </label>
              </th>
              <td>
                <input type="text" id="uwdgh_ga_conversion_tracking_id" name="uwdgh_ga_conversion_tracking_id" class="regular-text" value="<?php echo esc_attr(get_option('uwdgh_ga_conversion_tracking_id')); ?>" />
                <p class="description">e.g.: AW-XXXXXXXX</p>
                <p class="description">If you have different conversion tracking id than the main tracking id use this field. It will not be added as another global tag, but will be added as an additional gtag configuration function call.</p>
              </td>
            </tr>
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_ga_conversion_label">
                  <?php _e('Google Conversion Label (CONVERSION_LABEL):','uwdgh-ga-tracker');?>
                </label>
              </th>
              <td>
                <input type="text" id="uwdgh_ga_conversion_label" name="uwdgh_ga_conversion_label" class="regular-text" value="<?php echo esc_attr(get_option('uwdgh_ga_conversion_label')); ?>" />
                <p class="description">You can use conversion tracking to track when someone clicks a button or link on your website. You'll need both the AW-CONVERSION_ID and the CONVERSION_LABEL to do this.</p>
                <p class="description">This field only stores the CONVERSION_LABEL for reference purposes. The actual tracking scripts must be implementeted separately on your site.</p>
                <p class="description">See: <a href="https://support.google.com/tagmanager/answer/6105160?hl=en" title="Google Ads conversions - Tag Manager Help" target="_blank" rel="noopener noreferrer">Google Ads conversions - Tag Manager Help</a>
                  <br><a href="https://support.google.com/google-ads/answer/6331304?hl=en" title="Track clicks on your website as conversions - Google Ads Help" target="_blank" rel="noopener noreferrer">Track clicks on your website as conversions - Google Ads Help</a>
                </p>
              </td>
            </tr>
          </table>
          <?php submit_button(); ?>
        </form>
      </div>
    <?php }

    /**
    * Tracking code snippet
    */
    static function uwdgh_add_ga_tracker() {

			$_send_page_view = 'true';

			if ( !get_option('uwdgh_ga_tracking_enabled') ) {
				return;	// exit if not checked
			}

			$user = wp_get_current_user();
			if ( get_option('uwdgh_ga_tracking_admin_disabled') && is_user_logged_in() && ( in_array( 'administrator', (array) $user->roles ) || current_user_can('manage_options') ) ) {
				$_send_page_view = 'false';
			}
			
			if ( get_option('uwdgh_ga_tracking_user_disabled') && is_user_logged_in() ) {
				$_send_page_view = 'false';
			}

      ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script data-cookie-consent="tracking" async src="https://www.googletagmanager.com/gtag/js?id=<?php echo esc_attr(get_option('uwdgh_ga_tracking_id')); ?>"></script>
<script data-cookie-consent="tracking">
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', '<?php echo esc_attr(get_option('uwdgh_ga_tracking_id')); ?>', {send_page_view: <?php echo $_send_page_view; ?>});
<?php if (get_option('uwdgh_ga_conversion_tracking_id')) : ?>
gtag('config', '<?php echo esc_attr(get_option('uwdgh_ga_conversion_tracking_id')); ?>');
<?php endif; ?>
</script>
      <?php

    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_ga_tracker_uninstall() {
      delete_option('uwdgh_ga_tracking_id');
      delete_option('uwdgh_ga_conversion_tracking_id');
      delete_option('uwdgh_ga_conversion_label');
      delete_option('uwdgh_ga_tracking_enabled');
    }

  }

  UWDGH_GoogleAnalyticsTracker::init();

}
